---
title: "Opdracht2"
author: "Mathias Maes"
output: html_document
---

```{r, message=FALSE}
library(magrittr)
library(data.table)
library(knitr)
library(reticulate)
library(stevedata)
library(glmnet)
library(mice)

# Set the random seed, this will ensure the same random values are always generated.
set.seed(42)

opts_chunk$set(echo = TRUE)
```

## Data exploration

```{r}
data(ESSBE5)

ESSBE5 <- setDT(ESSBE5)

# Contains missing values,
#  thus this function errors.
#ESSBE5[, eduyrs] %>% density

```

## Data manipulation

### Complete data

`mice`:

The mice package implements a method to deal with missing data. The package creates multiple imputations (replacement values) for multivariate missing data. The method is based on Fully Conditional Specification, where each incomplete variable is imputed by a separate model. The MICE algorithm can impute mixes of continuous, binary, unordered categorical and ordered categorical data. In addition, MICE can impute continuous two-level data, and maintain consistency between imputations by means of passive imputation. Many diagnostic plots are implemented to inspect the quality of the imputations.

Generates Multivariate Imputations by Chained Equations (MICE)

https://www.rdocumentation.org/packages/mice/versions/3.13.0/topics/mice



```{r}
dat <- ESSBE5[, .(agea, female, eduyrs, hincfel, plcpvcr, trstplc)] %>%
   mice %>% complete %>% 
   setDT
```

### Split data

Randomly split the data in training data(60%) and test data(40%).

The columns `agea`, `female`, `eduyrs`, `hincfel` and `plcpvcr` are the input $\mathcal{X}$,
while the column `trstplc` is the output $\mathcal{Y}$.

```{r}
trn <- runif(nrow(ESSBE5)) < .6

x_trn <- dat[trn, .(agea, female, eduyrs, hincfel, plcpvcr)] %>% as.matrix
y_trn <- dat[trn, trstplc]
x_tst <- dat[!trn, .(agea, female, eduyrs, hincfel, plcpvcr)] %>% as.matrix
y_tst <- dat[!trn, trstplc]
```

## Machine Learning

**Create models with glmnet:**

`glmnet`:

Lasso and Elastic-Net Regularized Generalized Linear Models

We provide extremely efficient procedures for fitting the entire lasso or elastic-net regularization path for linear regression (gaussian), multi-task gaussian, logistic and multinomial regression models (grouped or not), Poisson regression and the Cox model. The algorithm uses cyclical coordinate descent in a path-wise fashion.

https://www.rdocumentation.org/packages/glmnet/versions/4.1-2

`cv.glmnet`:

Does k-fold cross-validation for glmnet, produces a plot, and returns a value for lambda (and gamma if `relax=TRUE`)

https://www.rdocumentation.org/packages/glmnet/versions/4.1-2/topics/cv.glmnet


```{r}
model_1 <- glmnet(x_trn, y_trn)
model_1 %>% plot(lab = TRUE)

model_2 <- cv.glmnet(x_trn, y_trn)
model_2 %>% plot

lambda = model_2$lambda.1se
```

## Predictions

Run predictions
```{r}
res <- predict(model_2, x_tst, lambda = lambda) %>% cbind(y_tst) %>%
   as.data.table %>% set_names(c("Y_Hat", "Y_Test"))
```

## RMSE

Calculate error of model based on test data:

$$
RMSE(x1, x2) = \sqrt{ \frac{\sum_{i=1}^{n} \left(x_{1, i} - x_{2,i}\right)^{2}}{n} } = \sqrt{mean\left( (x_{1} - x_{2})^2  \right)}
$$


```{r}
rmse <- function(x1, x2)
{
    diff_2 <- (x1 - x2)^2
    return(sqrt(mean(diff_2)))
}

res[, RMSE := rmse(Y_Hat, Y_Test)]

# Display results
res
```


## Conclusion

We zien dat ons getrained model niet zeer goed presteerd met een gemiddelde
error van bijna 2. Dit kan betekenen dat ons model niet goed is, we te weinig
data rijen hebben om een goed model te maken, dat we een te kort aan data hebben
(bv. misschien is 'aantal keer gearresteerd' ook een belangrijke factor) of een
laatste factor kan zijn dat er geen verband is met de input en de output.







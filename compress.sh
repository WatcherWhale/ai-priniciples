#!/usr/bin/env bash

for f in Labo*
do
    rm -f $f/*.html
    bsdtar -a -cf "$f.zip" $f/*
done
